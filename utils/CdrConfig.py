import ConfigParser

class CdrConfig:
	def __init__(self):
		self.config = ConfigParser.ConfigParser()
		self.config.read('parser_config.conf')

	def get(self,section,key):
		return self.config.get(section,key)

	def getint(self,section,key):
		return self.config.getint(section,key)



