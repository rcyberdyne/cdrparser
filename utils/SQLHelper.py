import psycopg2
import psycopg2.extras
from CdrLogger import CdrLogger

class SQLHelper:
	def __init__(self,config):
		self.sqlHost = config.get('SQL', 'host')
		self.sqlDb = config.get('SQL', 'db')
		self.sqlUser = config.get('SQL', 'username')
		self.sqlPassword = config.get('SQL', 'password')
		self.sqlPort = config.get('SQL', 'port')
		self.connection = None
		self.cursor = None
		self.logger = CdrLogger(config.get('Main','log_file'))

	def connect(self):
		success = True
		if self.connection is None:
			try:
				conn_string = "host='{}' dbname='{}' user='{}' password='{}' port='{}' ".format(self.sqlHost,self.sqlDb,self.sqlUser,self.sqlPassword,self.sqlPort)
				self.connection = psycopg2.connect(conn_string)
				self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
			except Exception,e:
			 	success = False
			 	self.logger.debug('SQL Helper Error: ' + str(e))
	 	return success


 	def __executeQuery(self, queryStr, queryParam):
 		success = True
 		if self.connection is not None and self.cursor is not None:
 			if queryStr is not None and len(queryStr) > 0:
 				try:
 					if queryParam is not None:
 						self.cursor.execute(queryStr, queryParam)
 					else:
 						self.cursor.execute(queryStr)
 				except Exception, e:
 					success = False
 					self.rollback()
 					self.logger.debug('SQL Helper Error on query ('+queryStr+') data ('+str(queryParam)+'): ' + str(e))
 			else:
 				success = False
 				self.logger.debug('SQL Helper: Provide a valid query string')	
 		else:
 			success = False
 			self.logger.debug('SQL Helper: Call connect before doing a query')
 		return success
 		
	def query(self,queryStr, queryParam):
		return self.__executeQuery(queryStr,queryParam)

	def queryWithResult(self,queryStr, queryParam):
		if self.__executeQuery(queryStr,queryParam):
			return self.cursor
		else:
			return None

	def commit(self):
		if  self.connection is not None:
			self.connection.commit()

	def rollback(self):
		if  self.connection is not None:
			self.connection.rollback()

	def close(self):
		if self.cursor is not None:
			self.cursor.close()
		if self.connection is not None:
			self.connection.close()




