import logging
import logging.handlers

class CdrLogger:
	def __init__(self,logFileName):
		self.logger = logging.getLogger('CDRLogger')

		#limit only to one logger handler so we don't see multiple log entries on the same string
		if len(self.logger.handlers) == 0:
			self.logger.setLevel(logging.DEBUG)
			handler = logging.handlers.RotatingFileHandler(logFileName, maxBytes=10485760, backupCount=100)
			fmt = logging.Formatter('[%(asctime)s] %(message)s')
			handler.setFormatter(fmt)
			self.logger.addHandler(handler)

	def debug(self,str):
		self.logger.debug(str)
