import threading
import psycopg2
import time
import decimal
import string

from utils.CdrLogger import CdrLogger
from utils.SQLHelper import SQLHelper
from utils.CdrConfig import CdrConfig


def doQuery(dbConn, query, data):
    resultCursor = dbConn.queryWithResult(query,data)
    result = None
    if resultCursor is not None:
        result = resultCursor.fetchone()
    return result

# ERRORS that constantly occurs
# 1. No client found for ip ( more than 95%) -- data issue, currently non existing
# 2. destination number with non numeric
# 3. no codec_name (read_code and write_codec is null) --> set codec to empty string if null
# 4. codedeck_code is not a number

#emptying call results
# delete from call_meta;
# delete from calls;
# delete from error_cdrs;

class CDRParser(threading.Thread):
    def __init__(self, queue,logger):
        threading.Thread.__init__(self)
        self.queue = queue
        self.config = CdrConfig()
        self.logger = logger
        self.dbConn = SQLHelper(self.config)

        errorCdrColumns = ['network_addr',
                      'direction',
                      'caller_id_name',
                      'caller_id_number',
                      'destination_number',
                      'context',
                      'start_stamp',
                      'answer_stamp',
                      'end_stamp',
                      'duration',
                      'billsec',
                      'hangup_cause',
                      'aleg_uuid',
                      'bleg_uuid',
                      'read_codec',
                      'write_codec',
                      'sip_req_uri',
                      'last_arg',
                      'sip_profile_name',
                      'sip_from_host',
                      'sip_to_host',
                      'progresssec',
                      'progress_mediasec',
                      'reason_id',
                      'client_id',
                      'codedeck_codes_id',
                      'rates_id']
        self.hangupCausesList = {}

        self.hangupCausesList['NORMAL_CLEARING']           = 1;
        self.hangupCausesList['NORMAL_UNSPECIFIED']        = 2;
        self.hangupCausesList['ORIGINATOR_CANCEL']         = 3;
        self.hangupCausesList['EXCHANGE_ROUTING_ERROR']    = 4;
        self.hangupCausesList['INCOMPATIBLE_DESTINATION']  = 5;
        self.hangupCausesList['MEDIA_TIMEOUT']             = 6;
        self.hangupCausesList['NORMAL_TEMPORARY_FAILURE']  = 7;
        self.hangupCausesList['INTERWORKING']              = 8;
        self.hangupCausesList['RECOVERY_ON_TIMER_EXPIRE']  = 9;
        self.hangupCausesList['NO_ANSWER']                 = 10;
        self.hangupCausesList['NORMAL_CIRCUIT_CONGESTION'] = 11;
        self.hangupCausesList['DESTINATION_OUT_OF_ORDER']  = 12;
        self.hangupCausesList['NETWORK_OUT_OF_ORDER']      = 13;
        self.hangupCausesList['NO_USER_RESPONSE']          = 14;
        self.hangupCausesList['PROTOCOL_ERROR']            = 15;
        self.hangupCausesList['UNKNOWN']                   = 16;
        self.hangupCausesList['USER_BUSY']                 = 17;
        self.hangupCausesList['MANDATORY_IE_MISSING']      = 18;
        self.hangupCausesList['BEARERCAPABILITY_NOTIMPL']  = 19;
        self.hangupCausesList['UNALLOCATED_NUMBER']        = 20;
        self.hangupCausesList['SWITCH_CONGESTION']         = 21;
        self.hangupCausesList['REQUESTED_CHAN_UNAVAIL']    = 22;
        self.hangupCausesList['SUBSCRIBER_ABSENT']         = 23;
        self.hangupCausesList['NO_ROUTE_DESTINATION']      = 24;
        self.hangupCausesList['INVALID_NUMBER_FORMAT']     = 25;
        

        self.errorCdrColumnsStr = ', '.join([str(column) for column in errorCdrColumns])

    def __createErrorCdr(self,cdr, reasonId, clientId, codedeck_codes_id, rates_id):
        #create a reason
        # reasonQuery = "INSERT INTO reason_causes (reason_text) VALUES ('"+reasonText+"') RETURNING reason_id;"
        # reasonResult = self.dbConn.queryWithResult(reasonQuery,None)

        # if reasonResult is not None:
        # reasonId = reasonResult.fetchone()['reason_id']
        errorCdrQuery = "INSERT INTO error_cdrs ("+self.errorCdrColumnsStr+") VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
        data = (  cdr['network_addr'],
                  cdr['direction'],
                  cdr['caller_id_name'],
                  cdr['caller_id_number'],
                  cdr['destination_number'],
                  cdr['context'],
                  cdr['start_stamp'],
                  cdr['answer_stamp'],
                  cdr['end_stamp'],
                  cdr['duration'],
                  cdr['billsec'],
                  cdr['hangup_cause'],
                  cdr['aleg_uuid'],
                  cdr['bleg_uuid'],
                  cdr['read_codec'],
                  cdr['write_codec'],
                  cdr['sip_req_uri'],
                  cdr['last_arg'],
                  cdr['sip_profile_name'],
                  cdr['sip_from_host'],
                  cdr['sip_to_host'],
                  cdr['progresssec'],
                  cdr['progress_mediasec'],
                  reasonId,
                  clientId,
                  codedeck_codes_id,
                  rates_id)
        if self.dbConn.query(errorCdrQuery,data):
            self.dbConn.commit()
        else:
            self.dbConn.rollback()
        # else:
        #     self.dbConn.rollback()


              
    def __setToNoneIfEmpty(self, data):
        data = data.replace('"','')
        if data is None or len(data) == 0:
            data = None
    
        return data

    def __getClientId(self, networkAddr, direction):
        result = None
        if networkAddr is not None:
            query = "SELECT * FROM client_ips WHERE client_ip = %s and direction= %s;"
            result = doQuery(self.dbConn, query, (networkAddr,direction))
        return result

    def __getCodeDeckId(self, clientId):
        query = "SELECT codedeck_id FROM codedeck WHERE client_id = "+str(clientId)
        return doQuery(self.dbConn, query, None)

    def __getCodeDeckCode(self, destinationNumber, codeDeckId):
        result = None
        if destinationNumber is not None:
            destinationNumberList = []
            ctr = 0
            while destinationNumber > 0:
                destinationNumberList.append(destinationNumber)
                destinationNumber = destinationNumber/10

            destinationNumberCsv = ','.join(str(d) for d in destinationNumberList)
            query = "SELECT id, codedeck_code FROM codedeck_codes WHERE codedeck_id = "+str(codeDeckId)+" AND codedeck_code IN("+destinationNumberCsv+") ORDER BY length DESC LIMIT 1;"
            result = doQuery(self.dbConn, query, None)
        return result

    def __getRateTable(self,direction, clientId):
        result = None
        if direction is not None:
            query = "SELECT ratetable_id FROM ratetable WHERE client_id = "+str(clientId)+ " AND ratetable_direction = " + str(direction)
            result = doQuery(self.dbConn, query, None)
        return result

    def __getRate(self, rateId, codeDeckCode, startCall):
        result = None
        
        if startCall is not None:
            codeDeckCode = int(codeDeckCode)
            codeDeckCodeList = []
            codeDeckCodeList.append(codeDeckCode)

            while codeDeckCode > 1:
                codeDeckCode = codeDeckCode/10
                codeDeckCodeList.append(codeDeckCode)
            
            codeDeckCodeCsv = ','.join(str(c) for c in codeDeckCodeList)
            startCall = str(startCall)

            query = "SELECT rates_id, rates_rate FROM rates WHERE ratetable_id = " + str(rateId) +" AND codedeck_code IN("+codeDeckCodeCsv+") " #ORDER BY rates_rate DESC LIMIT 1;"
            query = query + "AND rates_start_timeofday <= '" +startCall+ "' AND rates_end_timeofday >= '"+startCall+"' ORDER BY rates_rate DESC LIMIT 1;"
            result = doQuery(self.dbConn, query, None)
        return result

    def __createTableItem(self,name, idCol, cols, data):
        idResult = None

        if idCol is None:
            idResult = False

        if len(cols) == len(data):
            colStr = ', '.join([str(col) for col in cols])
            valString = "%s,"*len(data)
            query = "INSERT INTO "+name+" ("+colStr+") VALUES ("+valString[:-1]+")"

            if idCol is not None:
                query = query + " RETURNING "+idCol+";"

            result = self.dbConn.queryWithResult(query,data)

            if result is not None:
                if idCol is not None:
                  idResult = result.fetchone()[idCol]
                else:
                  idResult = True
            else:
                self.dbConn.rollback()
        else:
            self.logger.debug('ERROR __createTableItem: column and data length is not the same')

        return idResult

    def __getTableRowId(self, tableName, tableColId, whereClause):
        query = "SELECT {} FROM {} WHERE {}".format(tableColId, tableName, whereClause)
        return doQuery(self.dbConn, query, None)

    def __createCallEntry(self,cdr, cost, codeDeckCodeId, ratesId, clientId, ignoreBleg):
        #Create caller id names entry from cdr[caller_id_name] -- UNIQUE
        callerIdNameWhereClause = "caller_id_name = '{}' and client_id = {}".format(cdr['caller_id_name'],clientId)
        callerIdNameData = self.__getTableRowId('caller_id_names', 'caller_id_names_id', callerIdNameWhereClause)
        callerIdNameId = None

        if callerIdNameData is None:
            callerIdNameId = self.__createTableItem('caller_id_names', 'caller_id_names_id', ['caller_id_name', 'client_id'], (cdr['caller_id_name'],clientId))
            if callerIdNameId is None:
                msg = 'ERROR __createCallEntry: failed to create caller_id_names entry for client {}'.format(clientId)
                self.logger.debug(msg)
                return None
        else:
            callerIdNameId = callerIdNameData['caller_id_names_id']

        #Create caller id number entry from cdr[caller_id_number] --- UNIQUE
        callerIdNumberWhereClause = "caller_id_number = '{}' and client_id = {}".format(cdr['caller_id_number'],clientId)
        callerIdNumberData = self.__getTableRowId('caller_id_numbers', 'caller_id_number_id', callerIdNumberWhereClause)
        callerIdNumberId = None

        if callerIdNumberData is None:
            callerIdNumberId = self.__createTableItem('caller_id_numbers', 'caller_id_number_id', ['caller_id_number', 'client_id'], (cdr['caller_id_number'],clientId))
            if callerIdNumberId is None:
                msg = 'ERROR __createCallEntry: failed to create caller_id_numbers entry for client {}'.format(clientId) 
                self.logger.debug(msg)
                return None
        else:
            callerIdNumberId = callerIdNumberData['caller_id_number_id']

        #Create destination number entry from cdr[destination_number] --- UNIQUE
        detinationNumberIdWhereClause = "destination_number = '{}' and client_id = {}".format(cdr['destination_number'],clientId)
        detinationNumberData = self.__getTableRowId('destination_numbers', 'destination_number_id', detinationNumberIdWhereClause)
        detinationNumberId = None
        if detinationNumberData is None:
            detinationNumberId = self.__createTableItem('destination_numbers', 'destination_number_id', ['destination_number', 'client_id'], (cdr['destination_number'],clientId))
            if detinationNumberId is None:
                msg = 'ERROR __createCallEntry: failed to create destination_numbers entry for client {}'.format(clientId)
                self.logger.debug(msg)
                return None

        else:
            detinationNumberId = detinationNumberData['destination_number_id']

        hangupCauseId = self.hangupCausesList[cdr['hangup_cause']]
        if hangupCauseId is None:
            msg = 'ERROR __createCallEntry: failed to get hangup_causes entry where hangup cause is {}'.format(cdr['hangup_cause'])
            self.logger.debug(msg)
            return None

        #Create calls entry from
        callColumns = ['network_addr',
                       'direction',
                       'caller_id_names_id',
                       'caller_id_number_id',
                       'destination_number_id',
                       'codedeck_codes_id',
                       'client_id',
                       'start_stamp',
                       'answer_stamp',
                       'end_stamp',
                       'duration',
                       'billsec',
                       'aleg_uuid',
                       'bleg_uuid',
                       'hcause_id',
                       'sip_from_host',
                       'sip_to_host',
                       'rates_id',
                       'cost']

        callData = (cdr['network_addr'],
                    cdr['direction'],
                    callerIdNameId,
                    callerIdNumberId,
                    detinationNumberId,
                    codeDeckCodeId,
                    clientId,
                    cdr['start_stamp'],
                    cdr['answer_stamp'],
                    cdr['end_stamp'],
                    cdr['duration'],
                    cdr['billsec'],
                    cdr['aleg_uuid'],
                    cdr['bleg_uuid'],
                    hangupCauseId,
                    cdr['sip_from_host'],
                    cdr['sip_to_host'],
                    ratesId,
                    cost)

        #check if aleg is unique
        if cdr['aleg_uuid'] is not None:
            alegWhereClause = "aleg_uuid = '{}'".format(cdr['aleg_uuid'])
            alegData = self.__getTableRowId('calls', 'call_id', alegWhereClause)

            if alegData is not None:
                self.dbConn.rollback()
                return None
        else:
            self.dbConn.rollback()
            return (False,14)

        #check if bleg is unique
        if cdr['bleg_uuid'] is not None:
            blegWhereClause = "bleg_uuid = '{}'".format(cdr['bleg_uuid'])
            blegData = self.__getTableRowId('calls', 'call_id', blegWhereClause)
            if blegData is not None:
                self.dbConn.rollback()
                return (False,8)
        else:
            if not ignoreBleg:
                self.dbConn.rollback()
                return (False,15)

        callId = self.__createTableItem('calls', 'call_id', callColumns, callData)
        if callId is None:
            msg = 'ERROR __createCallEntry: failed to create calls entry for client {}'.format(clientId)
            self.logger.debug(msg)
            return None

        #Create call codec read entry from cdr[read_codec]
        read_codec =  cdr['read_codec'] if cdr['read_codec'] is not None else ""
        readCodecWhereClause = "codec_name = '{}'".format(read_codec)
        readCodecData = self.__getTableRowId('call_codecs', 'codec_id', readCodecWhereClause)
        readCodecId = None
        if readCodecData is None:
          readCodecId = self.__createTableItem('call_codecs', 'codec_id', ['codec_name'], (read_codec,))
          if readCodecId is None:
              msg = 'ERROR __createCallEntry: failed to create read codec call_codecs entry for client {}'.format(clientId)
              self.logger.debug(msg)
              return None
        else:
          readCodecId = readCodecData['codec_id']
        
        #Create call codec write entry from cdr[write_codec]
        write_codec =  cdr['write_codec'] if cdr['write_codec'] is not None else ""
        writeCodecWhereClause = "codec_name = '{}'".format(write_codec)
        writeCodecData = self.__getTableRowId('call_codecs', 'codec_id', writeCodecWhereClause)
        writeCodecId = None
        if writeCodecData is None:
          writeCodecId = self.__createTableItem('call_codecs', 'codec_id', ['codec_name'], (write_codec,))
          if writeCodecId is None:
              msg = 'ERROR __createCallEntry: failed to create write codec call_codecs entry for client {}'.format(clientId)
              self.logger.debug(msg)
              return None
        else:
          writeCodecId = writeCodecData['codec_id']

        #Create call meta entry
        callMetaColumn = ['call_id',
                          'codec_id_read',
                          'codec_id_write',
                          'context',
                          'sip_req_uri',
                          'last_arg',
                          'sip_profile_name',
                          'progress_sec',
                          'progress_media_sec']

        callMetaData = (callId,readCodecId,writeCodecId,cdr['context'],cdr['sip_req_uri'],cdr['last_arg'],cdr['sip_profile_name'],cdr['progresssec'],cdr['progress_mediasec'])

        callMetaCreated = self.__createTableItem('call_meta', None, callMetaColumn, callMetaData)
        if not callMetaCreated:
            msg = 'ERROR __createCallEntry: failed to create call_meta entry for client {}'.format(clientId)
            self.logger.debug(msg)
            return None

        self.dbConn.commit()
        return (True,None)

    def run(self):
        curr_ms = lambda: int(round(time.time() * 1000))
        self.logger.debug('thread started')

        if self.dbConn.connect():
            while True:
              # Get the work from the queue and expand the tuple
              cdr = self.queue.get()

              #if we got none, it means the processing of all unprocessed cdr is done
              if cdr is None:
                  self.dbConn.close()
                  self.queue.task_done()
                  break

              #step 1: Get Client
              if cdr['sip_from_host'] is not None:    
                  clientData = self.__getClientId(cdr['sip_from_host'],cdr['direction'])
                  if clientData is None: 
                      self.__createErrorCdr(cdr,1, None, None, None)
                      self.queue.task_done()
                      continue
              else:
                  self.__createErrorCdr(cdr,1, None, None, None)
                  self.queue.task_done()
                  continue
              
              #step 2: Check if client is ignored
              if clientData['ignore']:
                  self.logger.debug('network address {} with direction {} is ignored'.format(clientData['client_ip'],clientData['direction']))
                  self.queue.task_done()
                  continue

              #step 3: Get codedeck
              codeDeckData = self.__getCodeDeckId(clientData['client_id'])
              if codeDeckData is None:
                  self.__createErrorCdr(cdr,2, clientData['client_id'], None, None)  
                  self.queue.task_done()
                  continue                      

              #step 4: Get codedeck name and code
              codeDeckCodeData = None
              if cdr['destination_number'] is not None:
                  destinationNumber = None

                  if clientData['has_prefix']:
                      if clientData['client_has_number_sign']:
                          if '#' in cdr['destination_number']:
                            destinationNumberWithPrefix = cdr['destination_number'].split('#')
                            prefix = destinationNumberWithPrefix[0]
                            destinationNumber = destinationNumberWithPrefix[1]
                            if prefix !=  clientData['client_prefix']:
                                self.__createErrorCdr(cdr,9, clientData['client_id'], None, None)    
                                self.queue.task_done()
                                continue
                          else:
                                self.__createErrorCdr(cdr,10, clientData['client_id'], None, None)    
                                self.queue.task_done()
                                continue

                      else:
                          prefixLen = len(clientData['client_prefix'])
                          destNumLen = len(cdr['destination_number'])
                          if cdr['destination_number'][0:prefixLen] == clientData['client_prefix']:
                              destinationNumber = cdr['destination_number'][prefixLen:destNumLen]
                          else:
                              self.__createErrorCdr(cdr,9, clientData['client_id'], None, None)    
                              self.queue.task_done()
                              continue
                  else:
                      destinationNumber = cdr['destination_number']

                  try:
                      destinationNumber = filter(lambda c: c in string.digits , destinationNumber)
                      destinationNumber = int(destinationNumber)
                      cdr['destination_number'] = str(destinationNumber)
                  except ValueError as e:
                      self.logger.debug('Destination number {} error {}'.format(destinationNumber, e.value))
                      self.__createErrorCdr(cdr,11, clientData['client_id'], None, None)    
                      self.queue.task_done()
                      continue

                  codeDeckCodeData = self.__getCodeDeckCode(destinationNumber, codeDeckData['codedeck_id'])

                  if codeDeckCodeData is None:
                      self.__createErrorCdr(cdr,3, clientData['client_id'], None, None)    
                      self.queue.task_done()
                      continue
              else:
                  self.__createErrorCdr(cdr,11, clientData['client_id'], None, None)    
                  self.queue.task_done()
                  continue 

              #step 5: Find active rate table
              rateTable = None
              if cdr['direction'] is not None:
                  rateTable = self.__getRateTable(cdr['direction'], clientData['client_id'])
                  if rateTable is None:
                      cdrDirection = "True" if cdr['direction'] else "False"
                      errId = 5 if cdr['direction']  else 4
                      self.__createErrorCdr(cdr,errId, clientData['client_id'], codeDeckCodeData['id'], None)    
                      self.queue.task_done()
                      continue     
              else:
                  self.__createErrorCdr(cdr,12, clientData['client_id'], codeDeckCodeData['id'], None)    
                  self.queue.task_done()
                  continue     


              #step 6: Find the rate that matches the ratetable and codedeck code that is effective during the time of the call
              rate = None
              if cdr['start_stamp'] is not None:
                  rate = self.__getRate(rateTable['ratetable_id'], codeDeckCodeData['codedeck_code'], cdr['start_stamp'])
                  
                  if rate is None:
                      self.__createErrorCdr(cdr,6, clientData['client_id'], codeDeckCodeData['id'], None)    
                      self.queue.task_done()
                      continue

                  cost = "{0:.6f}".format((decimal.Decimal(cdr['billsec']) / 60) * decimal.Decimal(rate['rates_rate']))
                  createCallResult = self.__createCallEntry( cdr, 
                                                             cost, 
                                                             codeDeckCodeData['id'], 
                                                             rate['rates_id'], 
                                                             clientData['client_id'], 
                                                             clientData['ignore_bleg'])
                  
                  if createCallResult is not None:
                      if not createCallResult[0]:
                          self.__createErrorCdr(cdr, createCallResult[1], clientData['client_id'], codeDeckCodeData['id'], rate['rates_id'])    
              else:
                  self.__createErrorCdr(cdr,13, clientData['client_id'], None, None)    
                  self.queue.task_done()
                  continue

              self.queue.task_done()
        else:
              self.queue.task_done()




              