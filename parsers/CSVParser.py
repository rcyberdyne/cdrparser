import threading

import psycopg2
import os
import shutil
import time
import socket

from utils.CdrLogger import CdrLogger
from utils.SQLHelper import SQLHelper
from utils.CdrConfig import CdrConfig

class CSVParser(threading.Thread):
    def __init__(self, queue,logger):
        threading.Thread.__init__(self)
        self.queue = queue
        self.config = CdrConfig()
        self.logger = logger

        cdrColumns = ['network_addr',
                      'direction',
                      'caller_id_name',
                      'caller_id_number',
                      'destination_number',
                      'context',
                      'start_stamp',
                      'answer_stamp',
                      'end_stamp',
                      'duration',
                      'billsec',
                      'hangup_cause',
                      'aleg_uuid',
                      'bleg_uuid',
                      'read_codec',
                      'write_codec',
                      'sip_req_uri',
                      'last_arg',
                      'sip_profile_name',
                      'sip_from_host',
                      'sip_to_host',
                      'progresssec',
                      'progress_mediasec']
        self.cdrColumnsStr = ', '.join([str(column) for column in cdrColumns])

    def __setToNoneIfEmpty(self, data):
        data = data.replace('"','')
        if len(data) == 0:
            data = None
    
        return data
    def __isIp(self, ip):
        try:
          n = socket.inet_aton(ip)
          return True
        except Exception:
          return False

    def run(self):
        cdr_dir = self.config.get('Main', 'cdr_dir')
        dbConn = SQLHelper(self.config)
        curr_ms = lambda: int(round(time.time() * 1000))

        if dbConn.connect():
            while True:
              # Get the work from the queue and expand the tuple
              hostDir, cdrFile = self.queue.get()

              #if we got none, it means the processing of all files is done
              if hostDir is None and cdrFile is None:
                  dbConn.close()
                  self.queue.task_done()
                  break

              processedCdrDir = cdr_dir + "/" + hostDir + "/processed"
              errorCdrDir = cdr_dir + "/" + hostDir + "/error"

              try:
                  start = curr_ms()
                  fileLines = open(cdrFile).readlines()
                  for line in fileLines:
                      lineRow = line.rstrip().replace('","','"|"').replace(',','').split('|')        
                      sql_string = "INSERT INTO unprocessed_cdrs ("+self.cdrColumnsStr+") VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
                      ip = lineRow[22] if self.__isIp(lineRow[22]) else lineRow[0]
                      callerName = self.__setToNoneIfEmpty(lineRow[2])
                      if callerName is not None:
                          callerName = callerName.decode('iso-8859-1').encode('utf-8')
                      data = (
                        self.__setToNoneIfEmpty(lineRow[0]),
                        self.__setToNoneIfEmpty(lineRow[1].replace('inbound','0').replace('outbound','1')),
                        callerName,
                        self.__setToNoneIfEmpty(lineRow[3]),
                        self.__setToNoneIfEmpty(lineRow[4]),
                        self.__setToNoneIfEmpty(lineRow[5]),
                        self.__setToNoneIfEmpty(lineRow[6]),
                        self.__setToNoneIfEmpty(lineRow[7]),
                        self.__setToNoneIfEmpty(lineRow[8]),
                        self.__setToNoneIfEmpty(lineRow[9]),
                        self.__setToNoneIfEmpty(lineRow[10]),
                        self.__setToNoneIfEmpty(lineRow[11]),
                        self.__setToNoneIfEmpty(lineRow[12]),
                        self.__setToNoneIfEmpty(lineRow[13]),
                        self.__setToNoneIfEmpty(lineRow[15]),
                        self.__setToNoneIfEmpty(lineRow[16]),
                        self.__setToNoneIfEmpty(lineRow[17]),
                        self.__setToNoneIfEmpty(lineRow[19]),
                        self.__setToNoneIfEmpty(lineRow[20]),
                        self.__setToNoneIfEmpty(ip),
                        self.__setToNoneIfEmpty(lineRow[23]),
                        self.__setToNoneIfEmpty(lineRow[25]),
                        self.__setToNoneIfEmpty(lineRow[26])
                      )

                      if not dbConn.query(sql_string,data):
                          dbConn.rollback()

                  end = curr_ms() - start
                  shutil.move(cdrFile, processedCdrDir)
                  updateDurationSql = "update cdr_stat_process set cdr_processing_duration=cdr_processing_duration+%s where host=%s;"
                  updateFileCountSql = "update cdr_stat_process set cdr_currently_processed_file_count=cdr_currently_processed_file_count+%s where host=%s;"
                  dbConn.query(updateDurationSql, (end,hostDir))
                  dbConn.query(updateFileCountSql, (1,hostDir))
                  dbConn.commit()
              except Exception, e:
                  dbConn.rollback()
                  fileName = os.path.basename(cdrFile)
                  self.logger.debug("ERROR: on file {} host {}: {}".format(fileName,hostDir,str(e)))
                  shutil.move(cdrFile, errorCdrDir)
              self.queue.task_done()
        else:
              self.queue.task_done()