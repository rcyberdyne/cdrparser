import logging
import os
import Queue
import ConfigParser

#from CDRLogger import CDRLogger
from parsers.CSVParser import CSVParser
from utils.CdrLogger import CdrLogger
from utils.SQLHelper import SQLHelper
from utils.CdrConfig import CdrConfig

from time import time

# Add this to the crontab so this script will run every 12MN everyday
# 00 00 * * * python path/to/cdr_parser.py &

def createDirs(cdr_dir, hostDir):
    processedCdrDir = cdr_dir + "/" + hostDir + "/processed"
    errorCdrDir = cdr_dir + "/" + hostDir + "/error"

    if not os.path.exists(processedCdrDir):
        os.mkdir(processedCdrDir)
    if not os.path.exists(errorCdrDir):
        os.mkdir(errorCdrDir)

def endThreads(queue,threads,num_threads):
    for i in range(num_threads):
        queue.put((None,None))
    for t in threads:
        t.join()

def createStatEntry(host, fileCount, dbConn):
    deleteSql = "delete from cdr_stat_process where host='"+host+"';"
    insertSql = "insert into cdr_stat_process (host, cdr_file_count, cdr_currently_processed_file_count, cdr_processing_duration) values(%s,%s,%s,%s);"
    dbConn.query(deleteSql,None)
    dbConn.query(insertSql,(host,fileCount,0,0))
    dbConn.commit()

def main():
    cdrFileList = []
    queue = Queue.Queue()
    config = CdrConfig()
    logger = CdrLogger(config.get('Main','log_file'))
    dbConn = SQLHelper(config)

    if dbConn.connect():
        cdr_dir = config.get('Main', 'cdr_dir')
        num_threads=config.getint('Main', 'num_threads')
        threads = []

        #create parser threads
        for t in range(num_threads):
            parser = CSVParser(queue,logger)
            #parser.daemon = True
            parser.start()
            threads.append(parser)

        ts = time()
        totalFiles = 0
        for hostDir in os.listdir(cdr_dir):
            hostDirPath = cdr_dir + "/" + hostDir
            if os.path.isdir(hostDirPath):
                unprocessedDir = hostDirPath + "/unprocessed"
                if os.path.isdir(unprocessedDir):
                    unprocessedListDir = os.listdir(unprocessedDir)
                    fileCount = len(unprocessedListDir)
                    totalFiles = totalFiles + fileCount
                    createStatEntry(hostDir, fileCount, dbConn)
                    createDirs(cdr_dir, hostDir)
                    for cdrFile in unprocessedListDir:
                        cdrFilePath = unprocessedDir + "/" + cdrFile
                        if os.path.isfile(cdrFilePath):
                            queue.put((hostDir,cdrFilePath))
                        else:
                            logger.debug(cdrFilePath + " not a file, expecting cdr file")                        
            else:
                logger.debug(hostDir + " not a dir, expecting host directory")

        queue.join()
        endThreads(queue,threads,num_threads)
        dbConn.close()
        logger.debug('Parsing {} files lasted for {}s'.format(totalFiles, time() - ts))

if __name__ == '__main__':
    main()
