--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: chocolate_billing; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE chocolate_billing WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE chocolate_billing OWNER TO postgres;

\connect chocolate_billing

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: call_codecs_codec_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE call_codecs_codec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE call_codecs_codec_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: call_codecs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE call_codecs (
    codec_id integer DEFAULT nextval('call_codecs_codec_id_seq'::regclass) NOT NULL,
    codec_name character varying(20) NOT NULL
);


ALTER TABLE call_codecs OWNER TO postgres;

--
-- Name: call_meta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE call_meta (
    call_id integer NOT NULL,
    codec_id_read integer,
    codec_id_write integer,
    context character varying(255),
    sip_req_uri character varying(255),
    last_arg character varying(255),
    sip_profile_name character varying(32),
    progress_sec character varying(11),
    progress_media_sec character varying(11)
);


ALTER TABLE call_meta OWNER TO postgres;

--
-- Name: caller_id_names_caller_id_names_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE caller_id_names_caller_id_names_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE caller_id_names_caller_id_names_id_seq OWNER TO postgres;

--
-- Name: caller_id_names; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE caller_id_names (
    caller_id_names_id integer DEFAULT nextval('caller_id_names_caller_id_names_id_seq'::regclass) NOT NULL,
    caller_id_name character varying(30),
    client_id integer
);


ALTER TABLE caller_id_names OWNER TO postgres;

--
-- Name: caller_id_numbers_caller_id_number_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE caller_id_numbers_caller_id_number_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE caller_id_numbers_caller_id_number_id_seq OWNER TO postgres;

--
-- Name: caller_id_numbers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE caller_id_numbers (
    caller_id_number_id integer DEFAULT nextval('caller_id_numbers_caller_id_number_id_seq'::regclass) NOT NULL,
    caller_id_number character varying(30),
    client_id integer
);


ALTER TABLE caller_id_numbers OWNER TO postgres;

--
-- Name: calls_call_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE calls_call_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE calls_call_id_seq OWNER TO postgres;

--
-- Name: calls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE calls (
    call_id bigint DEFAULT nextval('calls_call_id_seq'::regclass) NOT NULL,
    network_addr inet,
    direction boolean,
    caller_id_names_id integer,
    caller_id_number_id integer,
    destination_number_id integer,
    codedeck_codes_id integer,
    client_id integer,
    start_stamp timestamp(6) without time zone,
    answer_stamp timestamp(6) without time zone,
    end_stamp timestamp(6) without time zone,
    duration integer,
    billsec integer,
    aleg_uuid uuid,
    bleg_uuid uuid,
    hcause_id integer,
    sip_from_host inet,
    sip_to_host inet,
    rates_id integer,
    cost numeric(15,6)
);


ALTER TABLE calls OWNER TO postgres;

--
-- Name: cdr_stat_process_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cdr_stat_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cdr_stat_process_id_seq OWNER TO postgres;

--
-- Name: cdr_stat_process; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cdr_stat_process (
    id integer DEFAULT nextval('cdr_stat_process_id_seq'::regclass) NOT NULL,
    host inet,
    cdr_file_count bigint,
    cdr_currently_processed_file_count bigint,
    cdr_processing_duration bigint
);


ALTER TABLE cdr_stat_process OWNER TO postgres;

--
-- Name: client_contact_information; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE client_contact_information (
    client_id integer,
    client_invoice_last_number smallint,
    client_settings_associated_gateways text,
    client_invoice_type character varying(45),
    client_invoice_template character varying(45),
    client_contact_email character varying(120),
    client_contact_address character varying(255),
    client_contact_tax_id character varying(45),
    client_contact_reg_id character varying(45),
    client_contact_banking_information character varying(45),
    client_settings_allowed_credit character varying(45),
    client_company_logo bytea
);


ALTER TABLE client_contact_information OWNER TO postgres;

--
-- Name: client_ips_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE client_ips_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_ips_id_seq OWNER TO postgres;

--
-- Name: client_ips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE client_ips (
    id integer DEFAULT nextval('client_ips_id_seq'::regclass) NOT NULL,
    client_id integer NOT NULL,
    client_ip inet NOT NULL,
    client_prefix character varying(20),
    client_has_number_sign boolean DEFAULT false NOT NULL,
    direction boolean NOT NULL,
    client_ip_string character varying(255) NOT NULL,
    ignore boolean DEFAULT false,
    has_prefix boolean DEFAULT false,
    ignore_bleg boolean DEFAULT false
);


ALTER TABLE client_ips OWNER TO postgres;

--
-- Name: clients_client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE clients_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clients_client_id_seq OWNER TO postgres;

--
-- Name: clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE clients (
    client_id integer DEFAULT nextval('clients_client_id_seq'::regclass) NOT NULL,
    client_name character varying(255) NOT NULL,
    client_status character varying(255) NOT NULL,
    reseller_id integer
);


ALTER TABLE clients OWNER TO postgres;

--
-- Name: codedeck_codedeck_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE codedeck_codedeck_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE codedeck_codedeck_id_seq OWNER TO postgres;

--
-- Name: codedeck; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE codedeck (
    codedeck_id integer DEFAULT nextval('codedeck_codedeck_id_seq'::regclass) NOT NULL,
    client_id integer NOT NULL,
    codedeck_name character varying(82) NOT NULL,
    active boolean
);


ALTER TABLE codedeck OWNER TO postgres;

--
-- Name: codedeck_codes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE codedeck_codes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE codedeck_codes_id_seq OWNER TO postgres;

--
-- Name: codedeck_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE codedeck_codes (
    id integer DEFAULT nextval('codedeck_codes_id_seq'::regclass) NOT NULL,
    codedeck_id integer,
    codedeck_code integer,
    codedeck_code_name character varying(255),
    length smallint
);


ALTER TABLE codedeck_codes OWNER TO postgres;

--
-- Name: destination_numbers_destination_number_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE destination_numbers_destination_number_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE destination_numbers_destination_number_id_seq OWNER TO postgres;

--
-- Name: destination_numbers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE destination_numbers (
    destination_number_id integer DEFAULT nextval('destination_numbers_destination_number_id_seq'::regclass) NOT NULL,
    destination_number character varying(30) NOT NULL,
    client_id integer
);


ALTER TABLE destination_numbers OWNER TO postgres;

--
-- Name: error_cdrs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE error_cdrs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE error_cdrs_id_seq OWNER TO postgres;

--
-- Name: error_cdrs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE error_cdrs (
    id integer DEFAULT nextval('error_cdrs_id_seq'::regclass) NOT NULL,
    network_addr inet,
    direction boolean,
    caller_id_name character varying(30),
    caller_id_number character varying(30),
    codedeck_codes_id integer,
    rates_id integer,
    destination_number character varying(30),
    context character varying(20),
    start_stamp timestamp(6) without time zone,
    answer_stamp timestamp(6) without time zone,
    end_stamp timestamp(6) without time zone,
    duration integer,
    billsec integer,
    hangup_cause character varying(50),
    aleg_uuid uuid,
    bleg_uuid uuid,
    read_codec character varying(20),
    write_codec character varying(20),
    sip_req_uri character varying(255),
    last_arg character varying(255),
    sip_profile_name character varying(20),
    sip_from_host inet,
    sip_to_host inet,
    progresssec character varying(11) DEFAULT '0'::character varying,
    progress_mediasec character varying(11) DEFAULT '0'::character varying,
    reason_id integer,
    client_id integer
);


ALTER TABLE error_cdrs OWNER TO postgres;

--
-- Name: hangup_causes_hcauses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hangup_causes_hcauses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hangup_causes_hcauses_id_seq OWNER TO postgres;

--
-- Name: hangup_causes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hangup_causes (
    hcauses_id integer DEFAULT nextval('hangup_causes_hcauses_id_seq'::regclass) NOT NULL,
    hcause_name character varying(50) NOT NULL
);


ALTER TABLE hangup_causes OWNER TO postgres;

--
-- Name: processed_cdrs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE processed_cdrs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE processed_cdrs_id_seq OWNER TO postgres;

--
-- Name: rates_rates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rates_rates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rates_rates_id_seq OWNER TO postgres;

--
-- Name: rates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE rates (
    rates_id integer DEFAULT nextval('rates_rates_id_seq'::regclass) NOT NULL,
    ratetable_id integer,
    codedeck_code integer,
    rates_rate numeric(5,5),
    rates_bill_increment smallint,
    rates_start_timeofday timestamp(6) without time zone,
    rates_end_timeofday timestamp(6) without time zone,
    rates_min bigint
);


ALTER TABLE rates OWNER TO postgres;

--
-- Name: ratetable_ratetable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ratetable_ratetable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ratetable_ratetable_id_seq OWNER TO postgres;

--
-- Name: ratetable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ratetable (
    ratetable_id integer DEFAULT nextval('ratetable_ratetable_id_seq'::regclass) NOT NULL,
    client_id integer NOT NULL,
    ratetables_name character varying(82) NOT NULL,
    ratetable_currency character varying(3) DEFAULT 'USD'::character varying NOT NULL,
    reseller_id integer,
    ratetable_direction boolean NOT NULL,
    ratetable_active boolean DEFAULT false NOT NULL
);


ALTER TABLE ratetable OWNER TO postgres;

--
-- Name: reason_causes_reason_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reason_causes_reason_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reason_causes_reason_id_seq OWNER TO postgres;

--
-- Name: reason_causes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reason_causes (
    reason_id integer DEFAULT nextval('reason_causes_reason_id_seq'::regclass) NOT NULL,
    reason_text character varying(255) NOT NULL
);


ALTER TABLE reason_causes OWNER TO postgres;

--
-- Name: reseller_contact_information; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reseller_contact_information (
    reseller_id integer,
    reseller_invoice_last_number integer,
    reseller_company_logo bytea,
    reseller_settings_associated_gateways text,
    reseller_invoice_type character varying(45),
    reseller_invoice_template character varying(45),
    reseller_contact_email character varying(120),
    reseller_contact_address character varying(255),
    reseller_contact_tax_id character varying(45),
    reseller_contact_reg_id character varying(45),
    reseller_contact_banking character varying(45),
    reseller_settings_allowed_credit character varying(45)
);


ALTER TABLE reseller_contact_information OWNER TO postgres;

--
-- Name: resellers_reseller_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE resellers_reseller_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE resellers_reseller_id_seq OWNER TO postgres;

--
-- Name: resellers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE resellers (
    reseller_id integer DEFAULT nextval('resellers_reseller_id_seq'::regclass) NOT NULL,
    reseller_name character varying(45) NOT NULL,
    reseller_status boolean DEFAULT false NOT NULL
);


ALTER TABLE resellers OWNER TO postgres;

--
-- Name: unprocessed_cdrs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE unprocessed_cdrs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE unprocessed_cdrs_id_seq OWNER TO postgres;

--
-- Name: unprocessed_cdrs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE unprocessed_cdrs (
    id integer DEFAULT nextval('unprocessed_cdrs_id_seq'::regclass) NOT NULL,
    network_addr inet,
    direction boolean,
    caller_id_name character varying(30),
    caller_id_number character varying(30),
    destination_number character varying(30),
    context character varying(20),
    start_stamp timestamp(6) without time zone,
    answer_stamp timestamp(6) without time zone,
    end_stamp timestamp(6) without time zone,
    duration smallint,
    billsec smallint,
    hangup_cause character varying(50),
    aleg_uuid uuid,
    bleg_uuid uuid,
    read_codec character varying(20),
    write_codec character varying(20),
    sip_req_uri character varying(255),
    last_arg character varying(255),
    sip_profile_name character varying(20),
    sip_from_host inet,
    sip_to_host inet,
    progresssec character varying(11),
    progress_mediasec character varying(11)
);


ALTER TABLE unprocessed_cdrs OWNER TO postgres;

--
-- Name: call_codecs_codec_id_codec_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY call_codecs
    ADD CONSTRAINT call_codecs_codec_id_codec_name_key UNIQUE (codec_id, codec_name);


--
-- Name: call_codecs_codec_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY call_codecs
    ADD CONSTRAINT call_codecs_codec_name_key UNIQUE (codec_name);


--
-- Name: call_codecs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY call_codecs
    ADD CONSTRAINT call_codecs_pkey PRIMARY KEY (codec_id);


--
-- Name: caller_id_names_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY caller_id_names
    ADD CONSTRAINT caller_id_names_pkey PRIMARY KEY (caller_id_names_id);


--
-- Name: caller_id_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY caller_id_numbers
    ADD CONSTRAINT caller_id_numbers_pkey PRIMARY KEY (caller_id_number_id);


--
-- Name: calls_aleg_uuid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_aleg_uuid_key UNIQUE (aleg_uuid);


--
-- Name: calls_bleg_uuid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_bleg_uuid_key UNIQUE (bleg_uuid);


--
-- Name: calls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_pkey PRIMARY KEY (call_id);


--
-- Name: cdr_stat_process_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cdr_stat_process
    ADD CONSTRAINT cdr_stat_process_pkey PRIMARY KEY (id);


--
-- Name: client_ips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client_ips
    ADD CONSTRAINT client_ips_pkey PRIMARY KEY (id);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (client_id);


--
-- Name: codedeck_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY codedeck_codes
    ADD CONSTRAINT codedeck_codes_pkey PRIMARY KEY (id);


--
-- Name: codedeck_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY codedeck
    ADD CONSTRAINT codedeck_pkey PRIMARY KEY (codedeck_id);


--
-- Name: destination_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY destination_numbers
    ADD CONSTRAINT destination_numbers_pkey PRIMARY KEY (destination_number_id);


--
-- Name: error_cdrs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY error_cdrs
    ADD CONSTRAINT error_cdrs_pkey PRIMARY KEY (id);


--
-- Name: hangup_causes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hangup_causes
    ADD CONSTRAINT hangup_causes_pkey PRIMARY KEY (hcauses_id);


--
-- Name: rates_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rates
    ADD CONSTRAINT rates_pkey PRIMARY KEY (rates_id);


--
-- Name: ratetable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ratetable
    ADD CONSTRAINT ratetable_pkey PRIMARY KEY (ratetable_id);


--
-- Name: reason_causes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reason_causes
    ADD CONSTRAINT reason_causes_pkey PRIMARY KEY (reason_id);


--
-- Name: resellers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resellers
    ADD CONSTRAINT resellers_pkey PRIMARY KEY (reseller_id);


--
-- Name: unprocessed_cdrs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unprocessed_cdrs
    ADD CONSTRAINT unprocessed_cdrs_pkey PRIMARY KEY (id);


--
-- Name: call_meta_fk_call_meta_calls1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX call_meta_fk_call_meta_calls1_idx ON call_meta USING btree (call_id);


--
-- Name: caller_id_names_id_caller_id_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX caller_id_names_id_caller_id_name ON caller_id_names USING btree (caller_id_name);


--
-- Name: caller_id_numbers_caller_id_number_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX caller_id_numbers_caller_id_number_id ON caller_id_numbers USING btree (caller_id_number);


--
-- Name: calls_fk_calls_clients1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX calls_fk_calls_clients1_idx ON calls USING btree (client_id);


--
-- Name: client_contact_information_fk_cli_cont_info1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX client_contact_information_fk_cli_cont_info1_idx ON client_contact_information USING btree (client_id);


--
-- Name: client_ips_fk_client_ips_clients1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX client_ips_fk_client_ips_clients1_idx ON client_ips USING btree (client_id);


--
-- Name: client_ips_idx_rates_0_0398UTZG7H; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_0398UTZG7H" ON client_ips USING btree (client_id, client_ip, client_prefix varchar_ops, client_has_number_sign);


--
-- Name: client_ips_idx_rates_0_42KUP9N4ME; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_42KUP9N4ME" ON client_ips USING btree (client_ip, client_prefix varchar_ops, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_6XLM84526T; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_6XLM84526T" ON client_ips USING btree (client_id, client_ip);


--
-- Name: client_ips_idx_rates_0_8V6MYHEHJ4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_8V6MYHEHJ4" ON client_ips USING btree (client_id, client_ip, client_prefix varchar_ops);


--
-- Name: client_ips_idx_rates_0_9A32O5IOIL; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_9A32O5IOIL" ON client_ips USING btree (client_prefix varchar_ops, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_9MS87HJNOX; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_9MS87HJNOX" ON client_ips USING btree (client_ip, client_prefix varchar_ops, client_has_number_sign);


--
-- Name: client_ips_idx_rates_0_D2QX2915GU; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_D2QX2915GU" ON client_ips USING btree (client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_FYKGMW16LY; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_FYKGMW16LY" ON client_ips USING btree (client_id, client_prefix varchar_ops, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_IYFBZIJEB5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_IYFBZIJEB5" ON client_ips USING btree (client_ip, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_KI63KYWSTF; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_KI63KYWSTF" ON client_ips USING btree (client_id, client_ip, client_prefix varchar_ops, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_LOC2P4FVZO; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_LOC2P4FVZO" ON client_ips USING btree (client_id, client_prefix varchar_ops, client_has_number_sign);


--
-- Name: client_ips_idx_rates_0_NNMIQI7YGY; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_NNMIQI7YGY" ON client_ips USING btree (client_id, client_ip, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_QZAG9I2HIR; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_QZAG9I2HIR" ON client_ips USING btree (client_id, client_has_number_sign, direction);


--
-- Name: client_ips_idx_rates_0_W5ACB1VL6A; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_W5ACB1VL6A" ON client_ips USING btree (client_prefix varchar_ops, client_has_number_sign);


--
-- Name: client_ips_idx_rates_0_WEF31WZV5H; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_0_WEF31WZV5H" ON client_ips USING btree (client_ip, client_prefix varchar_ops);


--
-- Name: client_ips_idx_rates_1_5BRQ3RVJ2J; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_5BRQ3RVJ2J" ON client_ips USING btree (client_id, client_prefix varchar_ops, direction);


--
-- Name: client_ips_idx_rates_1_74MAOZVLEG; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_74MAOZVLEG" ON client_ips USING btree (client_ip, client_has_number_sign);


--
-- Name: client_ips_idx_rates_1_9RYXPZ3Z3P; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_9RYXPZ3Z3P" ON client_ips USING btree (client_id, client_ip, client_has_number_sign);


--
-- Name: client_ips_idx_rates_1_K9U9Y45ABU; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_K9U9Y45ABU" ON client_ips USING btree (client_id, client_ip, client_prefix varchar_ops, direction);


--
-- Name: client_ips_idx_rates_1_NN9VYVG9FL; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_NN9VYVG9FL" ON client_ips USING btree (client_ip, client_prefix varchar_ops, direction);


--
-- Name: client_ips_idx_rates_1_OHT557UX3Z; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_OHT557UX3Z" ON client_ips USING btree (client_ip);


--
-- Name: client_ips_idx_rates_1_RQZVERWAFF; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_RQZVERWAFF" ON client_ips USING btree (client_prefix varchar_ops, direction);


--
-- Name: client_ips_idx_rates_1_SQA806MOUW; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_1_SQA806MOUW" ON client_ips USING btree (client_id, client_prefix varchar_ops);


--
-- Name: client_ips_idx_rates_2_FN7BBUNYFM; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_2_FN7BBUNYFM" ON client_ips USING btree (client_prefix varchar_ops);


--
-- Name: client_ips_idx_rates_2_JMM2CMCNRT; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_2_JMM2CMCNRT" ON client_ips USING btree (client_ip, direction);


--
-- Name: client_ips_idx_rates_2_P7E1O766KO; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_2_P7E1O766KO" ON client_ips USING btree (client_id, client_ip, direction);


--
-- Name: client_ips_idx_rates_2_WL8SS530QV; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_2_WL8SS530QV" ON client_ips USING btree (client_id, client_has_number_sign);


--
-- Name: client_ips_idx_rates_3_B791O3ICBY; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_3_B791O3ICBY" ON client_ips USING btree (client_has_number_sign);


--
-- Name: client_ips_idx_rates_3_SU1QIQ0ES7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_3_SU1QIQ0ES7" ON client_ips USING btree (client_id, direction);


--
-- Name: client_ips_idx_rates_4_AV80FM6P5G; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "client_ips_idx_rates_4_AV80FM6P5G" ON client_ips USING btree (direction);


--
-- Name: clients_fk_customers_resellers1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clients_fk_customers_resellers1_idx ON clients USING btree (reseller_id);


--
-- Name: clients_idx_rates_0_9R79LS5U4I; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_9R79LS5U4I" ON clients USING btree (client_id, client_name);


--
-- Name: clients_idx_rates_0_GG0TOGRQ13; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_GG0TOGRQ13" ON clients USING btree (client_name, client_status varchar_ops);


--
-- Name: clients_idx_rates_0_HFAQARM6KJ; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_HFAQARM6KJ" ON clients USING btree (client_name, client_status varchar_ops, reseller_id);


--
-- Name: clients_idx_rates_0_JIDGM304FO; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_JIDGM304FO" ON clients USING btree (client_id, client_name, client_status varchar_ops);


--
-- Name: clients_idx_rates_0_JN8F0B535J; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_JN8F0B535J" ON clients USING btree (client_id, client_status varchar_ops, reseller_id);


--
-- Name: clients_idx_rates_0_NE2WRDOBBU; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_NE2WRDOBBU" ON clients USING btree (client_status varchar_ops, reseller_id);


--
-- Name: clients_idx_rates_0_XFDPEIPQQ4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_0_XFDPEIPQQ4" ON clients USING btree (client_id, client_name, client_status varchar_ops, reseller_id);


--
-- Name: clients_idx_rates_1_DVGZA2E1CE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_1_DVGZA2E1CE" ON clients USING btree (client_name, reseller_id);


--
-- Name: clients_idx_rates_1_RH2IW2K72W; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_1_RH2IW2K72W" ON clients USING btree (client_id, client_name, reseller_id);


--
-- Name: clients_idx_rates_1_S69F5FROUC; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_1_S69F5FROUC" ON clients USING btree (client_id, client_status varchar_ops);


--
-- Name: clients_idx_rates_1_WQ6XYLWAPN; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_1_WQ6XYLWAPN" ON clients USING btree (client_name);


--
-- Name: clients_idx_rates_2_3T2BZIJEGZ; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_2_3T2BZIJEGZ" ON clients USING btree (client_id, reseller_id);


--
-- Name: clients_idx_rates_2_RMGZMB16FE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_2_RMGZMB16FE" ON clients USING btree (client_status varchar_ops);


--
-- Name: clients_idx_rates_3_XUP0GZ4YEM; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "clients_idx_rates_3_XUP0GZ4YEM" ON clients USING btree (reseller_id);


--
-- Name: codedeck_codes_fk_codedeck_codes_codedeck1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX codedeck_codes_fk_codedeck_codes_codedeck1_idx ON codedeck_codes USING btree (codedeck_id);


--
-- Name: codedeck_codes_idx_codedeck_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX codedeck_codes_idx_codedeck_code ON codedeck_codes USING btree (codedeck_code);


--
-- Name: codedeck_codes_idx_codedeck_length; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX codedeck_codes_idx_codedeck_length ON codedeck_codes USING btree (length);


--
-- Name: codedeck_codes_idx_codedeck_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX codedeck_codes_idx_codedeck_name ON codedeck_codes USING btree (codedeck_code_name, codedeck_code);


--
-- Name: codedeck_codes_idx_codedeck_namecodeid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX codedeck_codes_idx_codedeck_namecodeid ON codedeck_codes USING btree (codedeck_code_name, codedeck_code, codedeck_id, length);


--
-- Name: codedeck_fk_codedeck_clients1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX codedeck_fk_codedeck_clients1_idx ON codedeck USING btree (client_id);


--
-- Name: codedeck_idx_rates_0_A8ECB2DZY4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "codedeck_idx_rates_0_A8ECB2DZY4" ON codedeck USING btree (client_id, codedeck_name);


--
-- Name: codedeck_idx_rates_0_IMLYZ24ISA; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "codedeck_idx_rates_0_IMLYZ24ISA" ON codedeck USING btree (codedeck_id, client_id);


--
-- Name: codedeck_idx_rates_0_LS9Y875FT8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "codedeck_idx_rates_0_LS9Y875FT8" ON codedeck USING btree (codedeck_id, client_id, codedeck_name);


--
-- Name: codedeck_idx_rates_1_ODZ8SNO2X6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "codedeck_idx_rates_1_ODZ8SNO2X6" ON codedeck USING btree (codedeck_id, codedeck_name);


--
-- Name: codedeck_idx_rates_1_TI4QFNOXKP; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "codedeck_idx_rates_1_TI4QFNOXKP" ON codedeck USING btree (client_id);


--
-- Name: codedeck_idx_rates_2_010M78D31E; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "codedeck_idx_rates_2_010M78D31E" ON codedeck USING btree (codedeck_name);


--
-- Name: destination_numbers_idx_destination_number; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX destination_numbers_idx_destination_number ON destination_numbers USING btree (destination_number);


--
-- Name: hangup_causes_idx_hungup_cause; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX hangup_causes_idx_hungup_cause ON hangup_causes USING btree (hcause_name);


--
-- Name: rates_idx_rates_0_11VU5NY1ON; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_0_11VU5NY1ON" ON rates USING btree (codedeck_code, rates_rate);


--
-- Name: rates_idx_rates_0_DB21F1XI0F; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_0_DB21F1XI0F" ON rates USING btree (ratetable_id, codedeck_code);


--
-- Name: rates_idx_rates_0_URL521HVHT; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_0_URL521HVHT" ON rates USING btree (ratetable_id, codedeck_code, rates_rate);


--
-- Name: rates_idx_rates_0_YGYE6H6DFO; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_0_YGYE6H6DFO" ON rates USING btree (ratetable_id);


--
-- Name: rates_idx_rates_1_1SDQGIMUTC; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_1_1SDQGIMUTC" ON rates USING btree (ratetable_id, codedeck_code, rates_rate, rates_min);


--
-- Name: rates_idx_rates_1_722EQZEPTP; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_1_722EQZEPTP" ON rates USING btree (ratetable_id, rates_rate, rates_min);


--
-- Name: rates_idx_rates_1_7E9X2SPQFZ; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_1_7E9X2SPQFZ" ON rates USING btree (ratetable_id, rates_rate);


--
-- Name: rates_idx_rates_1_9K3B7KGPFJ; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_1_9K3B7KGPFJ" ON rates USING btree (codedeck_code);


--
-- Name: rates_idx_rates_1_NJIL10PI22; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_1_NJIL10PI22" ON rates USING btree (rates_rate, rates_min);


--
-- Name: rates_idx_rates_1_Q8UBFE5X2T; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_1_Q8UBFE5X2T" ON rates USING btree (codedeck_code, rates_rate, rates_min);


--
-- Name: rates_idx_rates_2_FVQJ3EWOBF; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_2_FVQJ3EWOBF" ON rates USING btree (ratetable_id, codedeck_code, rates_min);


--
-- Name: rates_idx_rates_2_N4267CSYCE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_2_N4267CSYCE" ON rates USING btree (codedeck_code, rates_min);


--
-- Name: rates_idx_rates_2_QK2DY55XGX; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_2_QK2DY55XGX" ON rates USING btree (rates_rate);


--
-- Name: rates_idx_rates_3_6C38PD7Y03; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_3_6C38PD7Y03" ON rates USING btree (ratetable_id, rates_min);


--
-- Name: rates_idx_rates_4_DA0NDIKAMS; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "rates_idx_rates_4_DA0NDIKAMS" ON rates USING btree (rates_min);


--
-- Name: ratetable_fk_ratetable_clients1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ratetable_fk_ratetable_clients1_idx ON ratetable USING btree (client_id);


--
-- Name: ratetable_fk_ratetable_resellers1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ratetable_fk_ratetable_resellers1_idx ON ratetable USING btree (reseller_id);


--
-- Name: reseller_contact_information_fk_res_con_info1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reseller_contact_information_fk_res_con_info1_idx ON reseller_contact_information USING btree (reseller_id);


--
-- Name: fk_call_meta_calls1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY call_meta
    ADD CONSTRAINT fk_call_meta_calls1 FOREIGN KEY (call_id) REFERENCES calls(call_id) ON UPDATE CASCADE;


--
-- Name: fk_caller_id_names_clients_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY caller_id_names
    ADD CONSTRAINT fk_caller_id_names_clients_1 FOREIGN KEY (client_id) REFERENCES clients(client_id);


--
-- Name: fk_caller_id_numbers_clients_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY caller_id_numbers
    ADD CONSTRAINT fk_caller_id_numbers_clients_1 FOREIGN KEY (client_id) REFERENCES clients(client_id);


--
-- Name: fk_calls_clients1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT fk_calls_clients1 FOREIGN KEY (client_id) REFERENCES clients(client_id) ON UPDATE CASCADE;


--
-- Name: fk_calls_hangup_causes_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT fk_calls_hangup_causes_1 FOREIGN KEY (hcause_id) REFERENCES hangup_causes(hcauses_id);


--
-- Name: fk_client_contact_information; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client_contact_information
    ADD CONSTRAINT fk_client_contact_information FOREIGN KEY (client_id) REFERENCES clients(client_id);


--
-- Name: fk_client_ips_clients1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client_ips
    ADD CONSTRAINT fk_client_ips_clients1 FOREIGN KEY (client_id) REFERENCES clients(client_id) ON UPDATE CASCADE;


--
-- Name: fk_clients_resellers1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT fk_clients_resellers1 FOREIGN KEY (reseller_id) REFERENCES resellers(reseller_id) ON UPDATE CASCADE;


--
-- Name: fk_codedeck_clients1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY codedeck
    ADD CONSTRAINT fk_codedeck_clients1 FOREIGN KEY (client_id) REFERENCES clients(client_id) ON UPDATE CASCADE;


--
-- Name: fk_codedeck_codes_codedeck1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY codedeck_codes
    ADD CONSTRAINT fk_codedeck_codes_codedeck1 FOREIGN KEY (codedeck_id) REFERENCES codedeck(codedeck_id) ON UPDATE CASCADE;


--
-- Name: fk_destination_numbers_clients_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY destination_numbers
    ADD CONSTRAINT fk_destination_numbers_clients_1 FOREIGN KEY (client_id) REFERENCES clients(client_id);


--
-- Name: fk_ratetable_clients1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ratetable
    ADD CONSTRAINT fk_ratetable_clients1 FOREIGN KEY (client_id) REFERENCES clients(client_id) ON UPDATE CASCADE;


--
-- Name: fk_ratetable_resellers1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ratetable
    ADD CONSTRAINT fk_ratetable_resellers1 FOREIGN KEY (reseller_id) REFERENCES resellers(reseller_id) ON UPDATE CASCADE;


--
-- Name: fk_resellers_reseller_contact_information_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reseller_contact_information
    ADD CONSTRAINT fk_resellers_reseller_contact_information_1 FOREIGN KEY (reseller_id) REFERENCES resellers(reseller_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

