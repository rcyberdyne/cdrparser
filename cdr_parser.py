import logging
import os
import Queue
import ConfigParser

#from CDRLogger import CDRLogger
from parsers.CDRParser import CDRParser
from utils.CdrLogger import CdrLogger
from utils.SQLHelper import SQLHelper
from utils.CdrConfig import CdrConfig

from time import time

# Add this to the crontab so this script will run every 12MN everyday
# 00 00 * * * python path/to/cdr_parser.py &

def endThreads(queue,threads,num_threads):
    for i in range(num_threads):
        queue.put(None)
    for t in threads:
        t.join()

def main():
    config = CdrConfig()
    logger = CdrLogger(config.get('Main','log_file'))
    dbConn = SQLHelper(config)

    if dbConn.connect():
        num_threads=config.getint('Main', 'num_threads')
        threads = []
        ts = time()
        totalFiles = 0
        query = """SELECT uc.* FROM unprocessed_cdrs AS uc 
                        LEFT JOIN client_ips AS ci ON uc.sip_from_host = ci.client_ip AND uc.direction = ci.direction 
                        LEFT JOIN codedeck as cd on ci.client_id = cd.client_id
                        WHERE ci.ignore = false and cd.active;"""
        results = dbConn.queryWithResult(query,None)
        if results is not None:
            queue = Queue.Queue()

            #create parser threads
            for t in range(num_threads):
                parser = CDRParser(queue,logger)
                #parser.daemon = True
                parser.start()
                threads.append(parser)

            row = results.fetchone()
            while row:
                queue.put(row)
                row = results.fetchone()

            queue.join()
            endThreads(queue,threads,num_threads)
        
        dbConn.close()
        logger.debug('Parsing {} unprocessed cdr items lasted for {}s'.format(results.rowcount,time() - ts))

if __name__ == '__main__':
    main()
